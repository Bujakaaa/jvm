package javadoc;

import java.util.List;
import java.util.Set;

/**
 * This class
 */
public class SDASchool implements School {

    /**
     * Add student given in a parameter to the SDA School.
     * @param student - student added to SDA School.
     */
        @Override
        public void addStudent (Student student){
    }


    /**
     * Create and add student to the SDA school. Method requires all parameters to create single student than adds newly created student to SDA school.
     * @param firstName - first name of the student that is going to be created.
     * @param lastName - last name of the student that is going to be created.
     * @param age - age of the student that is going to be created.
     * @param courses - courses attended by student that is going to be created.
     */
        @Override
        public void addStudent (String firstName, String lastName,int age, Set<Course > courses){
    }

    /**
     * Get student by first name and and last name. Method requires first name and last name to identify SDA student.
     * @param firstName - first name of the student that is going to be get.
     * @param lastName - first name of the student that is going to be get.
     * @return
     */
        @Override
        public Student getStudentByFirstNameAndLastName (String firstName, String lastName){
        return null;
    }
        @Override
        public List<Student> getStudentsByCourse (Course course){
        return null;
    }
}
