package javadoc;

import java.util.List;
import java.util.Set;

/**
 * This interface
 */

public interface School {

    /**
     * Add student given in a parameter to the school.
     * @param student - student added to school.
     */
    void addStudent(Student student);

    /**
     * Create and add student to the school. Method requires all parameters to create single student than adds newly created student to school.
     * @param firstName - first name of the student that is going to be created.
     * @param lastName - last name of the student that is going to be created.
     * @param age - age of the student that is going to be created.
     * @param courses - courses attended by student that is going to be created.
     */
    void addStudent(String firstName, String lastName, int age, Set<Course> courses);

    /**
     * Get student by first name and and last name. Method requires first name and last name to identify student.
     * @param firstName - first name of the student that is going to be get.
     * @param lastName - first name of the student that is going to be get.
     * @return returns student with this first and last name.
     */
    Student getStudentByFirstNameAndLastName(String firstName, String lastName);

    /**
     * Get list of students who attempt on unique course. Method requires course to get list of students.
     * @param course - course which list of student are attempting.
     * @return list of student who are attempting to this course.
     */
    List<Student> getStudentsByCourse(Course course);
}

