package javadoc;

import java.util.Set;

/**
 * This class presents Student.
 */

public class Student {
    private final String firstName;
    private final String lastName;
    private final int age;
    private final Set<Course> courses;

    /**
     * Class constructor creates Student with parameters:
     * @param firstName - Student's first name.
     * @param lastName - Student's last name.
     * @param age - Student's age.
     * @param courses Student's course.
     */
    public Student(String firstName, String lastName, int age, Set<Course> courses) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.courses = courses;
    }

    /**
     * Returns Student's first name.
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }


    /**
     * Returns Student's last name.
     * @return last name.
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * Returns Student's age (int).
     * @return age.
     */
    public int getAge() {
        return age;
    }


    /**
     * Returns Student's list of courses.
     * @return courses.
     */
    public Set<Course> getCourses() {
        return courses;
    }
}
