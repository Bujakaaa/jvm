package visualVM;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Ticket> ticketList = null;

        String komenda;
        do {
            System.out.println("Podaj komende");
            komenda=scanner.next();
            if (komenda.equalsIgnoreCase("dodaja")){
                int ilosc = scanner.nextInt();
                ticketList = dodajDoArrayList(ilosc);
            }

            else if (komenda.equalsIgnoreCase("dodajl")){
                int ilosc = scanner.nextInt();
                ticketList=dodajDoLinkedList(ilosc);
            }

            else  if (komenda.equalsIgnoreCase("czysc")){
                ticketList.clear();
            }
            else  if (komenda.equalsIgnoreCase("iteruj")){
                iteruj(ticketList);
                }

            else  if (komenda.equalsIgnoreCase("GC")){
                Runtime.getRuntime().gc(); // garbage collector
            }

        } while (!komenda.equalsIgnoreCase("quit"));

    }

    public  static void iteruj (List<Ticket> list) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).getId();
        }
    }

    public static List<Ticket> dodajDoArrayList (int liczba) {
        List<Ticket> ticketList = new ArrayList<>();
        for (int i = 0; i < liczba ; i++) {
            ticketList.add(new Ticket(i));
        }
        return ticketList;
    }

    public static List<Ticket> dodajDoLinkedList (int liczba) {
        List<Ticket> ticketList = new LinkedList<>();
        for (int i = 0; i < liczba ; i++) {
            ticketList.add(new Ticket(i));
        }
        return ticketList;
    }
}
